FROM ubuntu:16.04

EXPOSE 80
EXPOSE 60000-60300

# Update
RUN apt-get update && apt-get install -y python3 python3-pip nmap && apt-get install nano

# Install dependencies for Python with PIP package manager
RUN pip3 install --upgrade pip
RUN pip3 install requests

# Copy project to working direcotry 
COPY . /opt/hue-emulator

# Set the working directory to /app
WORKDIR /opt/hue-emulator

# OLD TRIES
# ENTRYPOINT /opt/hue-emulator/start-devices.sh; /bin/bash
# ENTRYPOINT /opt/hue-emulator/start-bridge.sh; /bin/bash
# Do not run! run this script via shell script ...
# RUN /opt/hue-emulator/start-bridge.sh