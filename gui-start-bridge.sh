#! /bin/bash

# turn of privious hue-emulator instances if any as 'service'
# sudo systemctl stop hue-emulator.service
# sudo systemctl disable hue-emulator.service 
# turn of privious hue-emulator instances if any as 'service'

# start as admin from python3 our Bridge Emulator
sudo python3 -u hue-bridge/HueEmulator3.py |& tee log/bridge-log.txt

# hold on to our console
$SHELL