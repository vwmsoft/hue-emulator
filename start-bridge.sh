#! /bin/bash

# turn of privious hue-emulator instances if any as 'service'
# sudo systemctl stop hue-emulator.service
# sudo systemctl disable hue-emulator.service 
# turn of privious hue-emulator instances if any as 'service'

# kill privious instances of this application
pkill -f hue-bridge/HueEmulator3.py

# start as admin from python3 our Bridge Emulator
python3 -u hue-bridge/HueEmulator3.py |& tee log/bridge-log.txt

# we dont need the process of this shell script anymore, kill it
pkill start-bridge.sh
pkill start-bridge.s

# hold on to our console
$SHELL