#! /bin/bash

sudo pkill '^python*'

# start as admin from python3 our hue devices
sudo python3 hue-emulator-ui-demo/HueEmulator.py

# hold on to our console
$SHELL