#! /bin/bash

# start as admin from python3 our hue devices
sudo python3 -u hue-device-manager/HueDeviceManager.py |& tee log/devices-log.txt

# hold on to our console
$SHELL