#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

Created on Tue June 5 11:33:07 2018

@author: velimir.avramovski

Main Hue Device Manager.

"""

# import my modules
import libs.readWrite.ReadSettings as ReadThings
import libs.generator.HueDevicesGenerator as Devices
                                         
# main function - gets called when we start this PY script
if __name__ == '__main__':
    
    print("Reading configs ...")   
    # read properties, supproted devices and devices ports/types we need to generate
    ReadThings.ReadConfigs()
    
    try:
        print("Creating devices ...")
        # for each device start a app listening and handeling requests to a GIVEN port
        Devices.GenerateDevices(ReadThings.devicesPort, ReadThings.devicesType)
    except KeyboardInterrupt:
        pass
      