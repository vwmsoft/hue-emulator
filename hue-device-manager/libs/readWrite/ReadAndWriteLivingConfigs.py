#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

Created on Tue June 5 11:33:07 2018

@author: velimir.avramovski

Reading and Writing live configs.

"""

import json
import os
import glob
import errno

def WriteThisConfigToDisk(configJson, path): # writes a json object as json file to a given path
    if not os.path.exists(os.path.dirname(path)):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    with open(path, 'w') as outfile:
        json.dump(configJson, outfile)
        
def ReadThisConfig(path): # reads json file from a path and returns a json object
    with open(path) as json_data:
       return json.load(json_data)        
   
def ClearLivingConfigsTree(path): # removing every living config of our devices
    files = glob.glob(path)
    for f in files:
        os.remove(f)

