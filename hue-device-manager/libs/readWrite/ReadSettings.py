#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

Created on Tue June 5 11:33:07 2018

@author: velimir.avramovski

Reading Properties files.

"""

import libs.static.ConstsAndGenerics as ConstsPaths
import json

separator = "="

supportedDevicesType = []
devices = []
devicesPort = []
devicesType = []

def ReadMainPropertiesFile(path): # reads the devices we want to create A.K.A folders names of the devices
    devices.clear()
    with open(path) as f:
        for line in f:
            if not (line is " "):
                if separator in line and checkIfValidLine(line):
                    name, value = line.split(separator, 1)
                    devices.append(str(value).rstrip('\r\n'))
    
def ReadSupportedDevices(): # converts our supported devices enum to a list 
    global supportedDevicesType
    supportedDevicesType = [e.value for e in ConstsPaths.DeviceType]
    
def GetInfosToGenerateDevice(): # creating our devices while validating data
    devicesPort.clear()
    devicesType.clear()
    with open(ConstsPaths.PropertiesFilePaths.PATH_TO_CREATE_DEVICES.value, 'r') as f:
        devicesArray = json.load(f)
    # print(type(devicesArray)) # type of dict name : parametar
    for device, parametars in devicesArray.items():
        # print(device)
        if parametars['active'] == True: # check if the device is active
            devicesPort.append(parametars['port'])
            devicesType.append(parametars['type'])

def checkIfValidLine(line): # check if line is a comment or a valid line
    if str(line).startswith("#"):
        return False
    else:
        return True

def ReadConfigs(): # main reader 
    # get supported devices - we do validation like this of input files
    ReadSupportedDevices()
    # read main property file where we store list of our devices
    ReadMainPropertiesFile(ConstsPaths.PropertiesFilePaths.PATH_TO_CREATE_DEVICES.value);
    # construct final list of devices to be created
    GetInfosToGenerateDevice()
    
    