#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

Created on Tue June 5 11:33:07 2018

@author: velimir.avramovski

Here we define some Constats and Paths to Files/Configs.

"""

import os
from enum import Enum

class DeviceType(Enum): # enum for the type of devices we use
    ON_OFF_LIGHT = "0x0000"
    DIMMABLE_LIGHT = "0x0100"
    COLOR_LIGHT = "0x0200"
    EXTENDED_COLOR_LIGHT = "0x0210"
    COLOR_TEMPERATURE_LIGHT = "0x0220"

class PropertiesFilePaths(Enum): # enum that creates a path to our properties/config files depending on the platform WIN/LINUX
    myOs = os.name
    cwd = os.getcwd()  
    # path to our init list of devices '../settings/devices.properties'
    if myOs == 'nt':
        # win
        PATH_TO_CREATE_DEVICES = '../settings/create-devices.json'
    else:
        # linux
        PATH_TO_CREATE_DEVICES = cwd + "/" + "settings" + "/" + "create-devices.json"
    if myOs == 'nt':
        # win
        PAHT_TO_GENERIC_CONFIGS = "genericConfigs/" + "{}" + "/" + 'config.json'
    else:
        # linux
        PAHT_TO_GENERIC_CONFIGS = cwd + "/" + "hue-device-manager/genericConfigs" + "/"  + "{}" + "/config.json"
        
class LivingDevicesConfigsPaths(Enum): # enum that creates a path to where we keep our living devices configs
    myOs = os.name
    cwd = os.getcwd()
    if myOs == 'nt':
        # win
        PATH_TO_LIVING_DEVICE_CONFIGS = "livingDevicesConfig/"
    else:
        # linux
        PATH_TO_LIVING_DEVICE_CONFIGS = cwd + "/" + "hue-device-manager" + "/" + "livingDevicesConfig/"
    