#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

Created on Tue June 6 11:33:07 2018

@author: velimir.avramovski

Generating Hue Devices.

"""

# add path to our lib folder ... specificly for tornado framework
import sys, os
myOs = os.name
cwd = os.getcwd()  
if myOs == 'nt':
    # win
    sys.path.append(cwd + "\\" + "libs")
else:
    # linux
    sys.path.append(cwd + "/" + "hue-device-manager" + "/" + "libs")
    
# webserver framework
import tornado.ioloop

# import my modules
import libs.static.ConstsAndGenerics as Generics
# devices servers and handlers
import libs.devices.Device0000 as device0000
import libs.devices.Device0100 as device0100
import libs.devices.Device0200 as device0200
import libs.devices.Device0210 as device0210
import libs.devices.Device0220 as device0220
    
def GenerateDevices(ports, types): # generate our devices
    howMuchDevices = len(ports)
    if howMuchDevices != len(types):
        print('Invalid inputs of ports and types of devices!')
        return -1
    else:
        # for each of port/deviceType generate the device we need to
        i = 0
        while not ( i == howMuchDevices):
            port = ports[i]
            typeD = types[i]
            i = i + 1
            if   (typeD == Generics.DeviceType.ON_OFF_LIGHT.value):
                print("Creating a device of type -> " + Generics.DeviceType.ON_OFF_LIGHT.value + " on port -> " + port)
                app = device0000.Device0000().Make0000()
                app.listen(port)
            elif (typeD == Generics.DeviceType.DIMMABLE_LIGHT.value):
                print("Creating a device of type -> " + Generics.DeviceType.DIMMABLE_LIGHT.value + " on port -> " + port)
                app = device0100.Device0100().Make0100()
                app.listen(port)
            elif (typeD == Generics.DeviceType.COLOR_LIGHT.value):
                print("Creating a device of type -> " + Generics.DeviceType.COLOR_LIGHT.value + " on port -> " + port)
                app = device0200.Device0200().Make0200()
                app.listen(port)
            elif (typeD == Generics.DeviceType.EXTENDED_COLOR_LIGHT.value):
                print("Creating a device of type -> " + Generics.DeviceType.EXTENDED_COLOR_LIGHT.value + " on port -> " + port)
                app = device0210.Device0210().Make0210()
                app.listen(port)
            elif (typeD == Generics.DeviceType.COLOR_TEMPERATURE_LIGHT.value):
                print("Creating a device of type -> " + Generics.DeviceType.COLOR_TEMPERATURE_LIGHT.value + " on port -> " + port)
                app = device0220.Device0220().Make0220()
                app.listen(port)
          
    # start the server
    try :
        ioloop = tornado.ioloop.IOLoop.instance()
        ioloop.make_current()
        ioloop.start()
    except RuntimeError as runError:
        print("status -> ")
        print(runError)
        