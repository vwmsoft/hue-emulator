#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

Created on Tue June 5 11:33:07 2018

@author: velimir.avramovski

Device 0x0100 - Dimmiable Light

"""

# webserver framework
import tornado.web
import json
import os.path

# import my modules
import libs.static.ConstsAndGenerics as Generics
import libs.readWrite.ReadAndWriteLivingConfigs as RWLivingConfigs

class Device0100():
    
    # define type of this device
    typeD = "0100"
    
    class Handler0100_Set(tornado.web.RequestHandler):
        def get(self): 
            
            print("----------------------------------------")
            port = str(self.request.host).split(":")[1]
            print("port = " + port)
            self.set_header('Content-Type', 'application/json')
            print("Handler" + Device0100.typeD + "_Set")     
            
            # get current status and return it 
            pathToOurConfig = Generics.LivingDevicesConfigsPaths.PATH_TO_LIVING_DEVICE_CONFIGS.value + port + ".json"         
            config_data = RWLivingConfigs.ReadThisConfig(pathToOurConfig)
                  
            # check request parametars, if it changing the state of the device do it
            if not self.get_argument("on", None) is None:
                stateOn = eval(self.get_argument("on", None))
                print("To set stateOn = " + str(stateOn)) 
                # change state of the device    
                config_data['state']['on'] = stateOn
                # save it again
                RWLivingConfigs.WriteThisConfigToDisk(config_data, pathToOurConfig)  
                stateTo = ("OK, state:" + str(config_data['state']['on']))
                print(stateTo)
                self.write((stateTo).encode('utf-8'))
                self.finish()  # Without this the client's request will hang
            
            print("----------------------------------------")
        
    class Handler0100_Get(tornado.web.RequestHandler):
        def get(self):  
            
            print("----------------------------------------")
            port = str(self.request.host).split(":")[1]
            print("port = " + port)
            self.set_header('Content-Type', 'application/json')
            print("Handler" + Device0100.typeD + "_Get")      
            
            # get current status and return it 
            pathToOurConfig = Generics.LivingDevicesConfigsPaths.PATH_TO_LIVING_DEVICE_CONFIGS.value + port + ".json"
            config_data = RWLivingConfigs.ReadThisConfig(pathToOurConfig)    
            
            if config_data['state']['reachable'] == True:
                n = json.dumps({'on': config_data['state']['on']})
                toReturn = json.loads(n)
                
                print("/get request -> " + str(toReturn))
                self.write(json.dumps(toReturn).encode('utf-8'))
                self.finish()  # Without this the client's request will hang
            
            print("----------------------------------------")

    class Handler0100_Origin(tornado.web.RequestHandler):
        def get(self):
            print("----------------------------------------")
            port = str(self.request.host).split(":")[1]
            print("port = " + port)
            print("Handler0100_Origin")
            print("----------------------------------------")
        
    class Handler0100_Detect(tornado.web.RequestHandler):
        def get(self): 

            print("----------------------------------------")
            port = str(self.request.host).split(":")[1]
            print("port = " + port)
            self.set_header('Content-Type', 'application/json')
            print("Handler" + Device0100.typeD + "_Detect")
            print("handling /detect request ...")
            
            # check if device is already created
            pathToSaveLivingConfig = Generics.LivingDevicesConfigsPaths.PATH_TO_LIVING_DEVICE_CONFIGS.value + port + ".json"
 
            if not os.path.exists(pathToSaveLivingConfig):
                pathToOurGenericConfig = str(Generics.PropertiesFilePaths.PAHT_TO_GENERIC_CONFIGS.value).format(Device0100.typeD)
                config_data = RWLivingConfigs.ReadThisConfig(pathToOurGenericConfig)
                # get this config, modify it to be unique, store it and send it to Hue Bridge
                config_data["uniqueid"] = config_data["uniqueid"] + "-" +  port
                print(config_data)
                RWLivingConfigs.WriteThisConfigToDisk(config_data, pathToSaveLivingConfig)
                self.write(json.dumps(config_data).encode('utf-8'))
                self.finish()  # Without this the client's request will hang
            else:
                # if device exists then get that living config and send it to the Bridge
                config_data = RWLivingConfigs.ReadThisConfig(pathToSaveLivingConfig)
                print(config_data)
                self.write(json.dumps(config_data).encode('utf-8'))
                self.finish()  # Without this the client's request will hang
            
            print("----------------------------------------")
        
    class Handler0100_ReachableToggle(tornado.web.RequestHandler):
        def get(self):
            
            print("----------------------------------------")
            port = str(self.request.host).split(":")[1]
            print("port = " + port)
            print("Handler0100_Reachable-Toggle")
            
            # get current status and return it 
            pathToOurConfig = Generics.LivingDevicesConfigsPaths.PATH_TO_LIVING_DEVICE_CONFIGS.value + port + ".json"
            config_data = RWLivingConfigs.ReadThisConfig(pathToOurConfig)    
            if config_data['state']['reachable'] is True:
                config_data['state']['reachable'] = False
            else:
                config_data['state']['reachable'] = True
            RWLivingConfigs.WriteThisConfigToDisk(config_data, pathToOurConfig)
            print("Reachable Toggled -> " + str(config_data['state']['reachable']))
            
            print("----------------------------------------")
    
    def Make0100(self):
        # returns a tornado web application with handlers on given endpoints
        return tornado.web.Application([
            (r"/", Device0100.Handler0100_Origin),
            (r"/detect", Device0100.Handler0100_Detect),
            (r"/get", Device0100.Handler0100_Get),
            (r"/set", Device0100.Handler0100_Set),  
            (r"/reachable-toggle", Device0100.Handler0100_ReachableToggle),  # custom request to set the light reachable/unreacheable
        ])
    
    