#!/usr/bin/python3

# -*- coding: utf-8 -*-

from tkinter import Tk
import libs.Draw as draw
    
# main function - gets called when we start this PY script
if __name__ == '__main__':
    
    window = Tk()
    draw.DrawApp(window)