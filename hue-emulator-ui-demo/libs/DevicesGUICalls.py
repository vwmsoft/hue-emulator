# -*- coding: utf-8 -*-

import subprocess, os, sys
import threading
import time as t
    
from tkinter import Tk, StringVar

from libs import DevicesGUI as devices
import libs.Const 

global areDevicesActive
areDevicesActive = False

def OnHueDevicesClicked():
    print("Activating Hue Simulated Devices ...")
    
    global areDevicesActive
    if not areDevicesActive:
        areDevicesActive = True
        
        # update devices status
        devices.updateDevicesStatus("online")
        
        # log output of the devices
        command = ['./gui-start-devices.sh']
        subprocess.Popen(command, shell=True)
        # Create new thread: pass target and argument
        threadReading = threading.Thread(target=readFromLogFile, args=())
        # Start new Threads
        threadReading.daemon=True
        threadReading.start()

def readFromLogFile():
    try :
        readLog = open(libs.Const.LogFilePaths.PATH_TO_LOG_DEVICES.value, "r")
        while(1):
            line = readLog.readline()
            if not (line is ""):
                devices.updateDevicesLogger(str(line))
            t.sleep(0.1)
    except FileNotFoundError as e:
        print(e)

def DeleteHueDevice():
    print("DeleteHueDevice")
  
def PauseHueDevice():
    print("PauseHueDevice")
    
def AddThisDevice():
    print("AddThisDevice")
    