# -*- coding: utf-8 -*-

import subprocess, os, sys
import threading
import time as t
import shutil
    
from tkinter import Tk, StringVar, messagebox

from libs import BridgeGUI as bridge
import libs.Const 

global isBridgeActive
isBridgeActive = False

def OnHueBridgeClicked():
    print("Activating Hue Emulated Bridge ...")
    
    global isBridgeActive
    if not isBridgeActive:
        isBridgeActive = True
        
        # update bridge status
        bridge.updateBridgeStatus("online")
        
        # log output of the bridge
        command = ['./gui-start-bridge.sh']
        subprocess.Popen(command, shell=True)
        # Create new thread: pass target and argument
        threadReading = threading.Thread(target=readFromLogFile, args=())
        # Start new Threads
        threadReading.daemon=True
        threadReading.start()

def readFromLogFile():
    try:
        readLog = open(libs.Const.LogFilePaths.PATH_TO_LOG_BRIDGE.value, "r")
        while(1):
            line = readLog.readline()
            if not (line is ""):
                bridge.updateBridgeLogger(str(line))
            t.sleep(0.1)
    except FileNotFoundError as e:
        print(e)

def ClearHueBridgeConfig():
    print("Cleaning Hue Bridge Config - DEFAULT config ")
    
    result = messagebox.askyesno("Cleaning", "Delete current Hue Bridge Config ?", icon='warning')
    if result == True:
        cwd = os.getcwd()  
        deleteFile = cwd + "/hue-bridge/config.json"
        replaceWithFile = cwd + "/hue-bridge/clean-config.json"
        os.remove(deleteFile)
        shutil.copy(replaceWithFile, deleteFile)
    else:
        print("...")