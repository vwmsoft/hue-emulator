# -*- coding: utf-8 -*-

import os
from enum import Enum

class LogFilePaths(Enum):
    myOs = os.name
    cwd = os.getcwd()  
    if myOs == 'nt':
        # win
        PATH_TO_LOG_BRIDGE = cwd + "/log/bridge-log.txt"
        PATH_TO_LOG_DEVICES = cwd + "/log/devices-log.txt"
    else:
        # linux
        PATH_TO_LOG_BRIDGE = cwd + "/log/bridge-log.txt"
        PATH_TO_LOG_DEVICES = cwd + "/log/devices-log.txt"