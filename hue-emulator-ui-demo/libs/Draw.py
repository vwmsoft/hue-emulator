# -*- coding: utf-8 -*-

from tkinter import Tk, Label, Button, Listbox, W, E, N, S, Frame

import libs.BridgeGUI as bridgeGUI
import libs.DevicesGUI as devicesGUI

class DrawApp():
    
    def __init__(self, window):
        self.window = window
        
        # window.geometry('350x200')
        window.title("Hue Emulator - by Velimir Avramovski @MusalaSoft")
        # center out grid's elements
        window.resizable(False, False)
        window.grid_rowconfigure(0, weight=1)
        window.grid_columnconfigure(0, weight=1)
        
        # start info frame
        topFrame = Frame(window)
        topFrame.grid_rowconfigure(0, weight=1)
        topFrame.grid_columnconfigure(0, weight=1)
        topFrame.grid(column=0, row=0, columnspan=4, rowspan=1, sticky=W+E+N+S, padx=(10, 10), pady=(10, 10))
        # info
        infoLabel = Label(topFrame, text="Hue Emulator GUI", font=("Arial Bold", 20))
        infoLabel.grid(column=0, row=0, columnspan=4, rowspan=1, sticky=W+E+N+S, padx=0, pady=(0, 0))
    
        # left column - hue bridge
        frameLeft = Frame(window)
        frameLeft.grid(column=0, row=1, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(10, 10), pady=(10, 10))
        bridgeGUI.HueBridgeGUI(frameLeft)
        
        # right column - hue devices
        frameRight= Frame(window)
        frameRight.grid(column=1, row=1, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(10, 10), pady=(10, 10))
        devicesGUI.HueDevicesGUI(frameRight)
         
        # start the loop
        window.mainloop()
        