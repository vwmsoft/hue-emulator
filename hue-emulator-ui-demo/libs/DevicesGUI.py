# -*- coding: utf-8 -*-

from tkinter import Tk, Label, Button, Listbox, W, E, N, S, Entry, StringVar, OptionMenu, scrolledtext, END
from libs import DevicesGUICalls as DevicesCalls

def HueDevicesGUI(window): # right column - hue devices  
    
    # list of devices
    devicesLabel = Label(window, text="Hue Simulate Devices")
    devicesLabel.grid(column=0, row=1, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(25, 15))
    devicesList = Listbox(window)
    # devicesList.insert(1, "Python")
    devicesList.grid(column=0, row=2, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    # delete and pause device
    deleteDevice = Button(window, text="Delete", command=DevicesCalls.DeleteHueDevice)
    deleteDevice.grid(column=0, row=3, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    pauseDevice = Button(window, text="Pause", command=DevicesCalls.PauseHueDevice)
    pauseDevice.grid(column=1, row=3, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    
    # add new device
    Label(window, text="Add new Device").grid(column=0, row=4, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(25, 15))
    # type of device
    OPTIONS = [
    "On/Off Light",
    "Dimmable Light",
    "Color Light",
    "Extended Color Light",
    "Temperature Color Light"
    ] 
    variable = StringVar(window)
    variable.set(OPTIONS[0]) # default value
    deviceTypeDropDown = OptionMenu(window, variable, *OPTIONS)
    deviceTypeDropDown.grid(column=0, row=5, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    # port 
    Label(window, text="Port").grid(column=0, row=7, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    entryPort = Entry(window)
    entryPort.grid(column=1, row=7, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    # add button 
    addThisDevice = Button(window, text="Add this Device", command=DevicesCalls.AddThisDevice)
    addThisDevice.grid(column=0, row=8, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    # add new device
    
    # start generated devices
    Label(window, text="Generate Devices").grid(column=0, row=9, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(25, 15))
    startHueDevices = Button(window, text="Start Hue Devices", command=DevicesCalls.OnHueDevicesClicked)
    startHueDevices.grid(column=0, row=10, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(0, 0))
    global devicesStatus
    devicesStatus = StringVar()
    devicesStatus.set("status : offline")
    hueDevicesStatus = Label(window, name="devicesStatus", textvariable=devicesStatus)
    # hueBridgeStatus.configure(background='magenta')
    hueDevicesStatus.grid(column=1, row=10, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    
    # logger label
    global devicesLoger
    devicesLoger = scrolledtext.ScrolledText(window, width=40, height=6)
    devicesLoger.grid(column=0, row=11, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(25, 0))
    devicesLoger.insert(END, "" + '\n', "")
    devicesLoger.yview(END)
    devicesLoger.configure(state='disabled')
    
def updateDevicesStatus(status):
    devicesStatus.set("status : " + status)
    
def updateDevicesLogger(toAdd):
    devicesLoger.configure(state='normal')
    devicesLoger.insert(END, toAdd, "")
    devicesLoger.yview(END)
    devicesLoger.configure(state='disabled')
    