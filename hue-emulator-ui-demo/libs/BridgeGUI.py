# -*- coding: utf-8 -*-

from tkinter import Tk, Label, Button, Listbox, W, E, N, S, scrolledtext, StringVar, END
from libs import BridgeGUICalls as BrdidgeCalls

def HueBridgeGUI(window): # left column - hue bridge
    
    # start label
    devicesLabel = Label(window, text="Hue Bridge")
    devicesLabel.grid(column=0, row=0, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(25, 15))
    
    # row 1
    startHueBridge = Button(window, text="Start Hue Bridge", command=BrdidgeCalls.OnHueBridgeClicked)
    # startHueBridge.configure(background='magenta')
    startHueBridge.grid(column=0, row=1, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    global bridgeStatus
    bridgeStatus = StringVar()
    bridgeStatus.set("status : offline")
    hueBridgeStatus = Label(window, name="bridgeStatus", textvariable=bridgeStatus)
    # hueBridgeStatus.configure(background='magenta')
    hueBridgeStatus.grid(column=1, row=1, columnspan=1, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=0)
    
    # row 2
    clearHueBridgConfig = Button(window, text="Clear Hue Bridge Config", command=BrdidgeCalls.ClearHueBridgeConfig)
    clearHueBridgConfig.grid(column=0, row=2, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(0, 0))
    # startHueBridge.configure(background='green')
    
    # logger label
    global bridgeLogger
    bridgeLogger = scrolledtext.ScrolledText(window, width=40, height=30)
    bridgeLogger.grid(column=0, row=3, columnspan=2, rowspan=1, sticky=W+E+N+S, padx=(0, 0), pady=(25, 0))
    bridgeLogger.insert(END, "" + '\n', "")
    bridgeLogger.yview(END)
    bridgeLogger.configure(state='disabled')

def updateBridgeStatus(status):
    bridgeStatus.set("status : " + status)
    
def updateBridgeLogger(toAdd):
    bridgeLogger.configure(state='normal')
    bridgeLogger.insert(END, toAdd, "")
    bridgeLogger.yview(END)
    bridgeLogger.configure(state='disabled')