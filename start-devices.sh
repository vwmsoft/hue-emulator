#! /bin/bash

# kill privious instances of this application
pkill -f hue-device-manager/HueDeviceManager.py

# start as admin from python3 our hue devices
python3 -u hue-device-manager/HueDeviceManager.py |& tee log/devices-log.txt

# we dont need the process of this shell script anymore, kill it
pkill start-devices.sh
pkill start-devices.s

# hold on to our console
$SHELL